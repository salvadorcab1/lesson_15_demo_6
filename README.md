## Requirements

- Create a K8s (it is recommended to use Terraform as described in the project)


## Steps:

## Create EKS Cluster with TF

1) Define the variables in **terraform.tfvars** file:

Example:

vpc_cidr_block = "10.0.0.0/16"
private_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
public_subnet_cidr_blocks = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

2) Initialize the module and verify the configuration is correct: 

- terraform init
- terraform plan


**NOTE:** After terraform init the module will be created inside the .terraform folder.


## Deploy NGINX in the cluster

1) Configure kubectl in order to access to the cluster. The requirements for this are:

   - AWS CLI installed.
   - kubectl installed
   - aws-iam-authenticator installed.

   1.1) Update kubeconfig file: 
   
   - aws eks update-kubeconfig --name <CLUSTER_NAME> --region <REGION>
   - Example: - aws eks update-kubeconfig --name myapp-eks-cluster --region eu-west-3

   1.2) Verify it worked by accessing to the nodes: - kubectl get node

   1.3) Verify there are no pods running: - kubectl get pod


3) Apply the nginx deployment-service file: - kubectl apply -f nginx-config.yaml


4) Verify it is running: - kubectl get pods


5) Verify in the browser using the LoadBalancer DNS. The DNS can be found in **AWS UI EC2 > Load Balancer** and choosing the load balancer


**NOTE:** The TF EKS deployment is configured to save a local copy of the kube config file (kubeconfig_myapp-eks-cluster). This file allows us to connect to the EKS cluster. This file will be used for Ansible to connect as well.

**NOTE:** If you are using Cygwin you can update the kube config file and then copy it into Cygwin. (In this case we use the name kubeconfig_myapp-eks-cluster):

        - aws eks update-kubeconfig --name <CLUSTER_NAME> --region <REGION>
        - Example: - aws eks update-kubeconfig --name myapp-eks-cluster --region eu-west-3

The config file is usually in ~/.kube/config.


## Ansible Configuration

1) Install the necessary requirements:

- /usr/bin/python3.8.exe -m pip install PyYAML --user
- /usr/bin/python3.8.exe -m pip install kubernetes --user
- /usr/bin/python3.8.exe -m pip install jsonpatch --user

**NOTE:** You can verify the module is installed: 

- python3 -c "import yaml"
- python3 -c "import kubernetes"
- python3 -c "import jsonpatch"

If there is no error (nor response) it means the module is installed.


2) Execute Ansible playbook: - ansible-playbook deploy-to-k8s.yaml


3) Verify it works: - export KUBECONFIG=~/projects/demo_6_k8s_deployment/terraform/kubeconfig_myapp-eks-cluster
                    - kubectl get ns         (Here we should see the created namespace myapp) 


## Set ENV Variable For Kubeconfig

1) In Ansible export kubeconfig variable: 

- export K8S_AUTH_KUBECONFIG=~/projects/demo_6_k8s_deployment/terraform/kubeconfig_myapp-eks-cluster


2) Run Ansible playbook:  - ansible-playbook deploy-to-k8s.yaml


## Deploy App in New Namespace

1) Execute Ansible: - ansible-playbook deploy-to-k8s.yaml


2) Verify the pods and services: 

- kubectl get pod -n my-app
- kubectl get svc -n my-app

![Screenshot of pipeline.](/images/pods.jpg)


3) Copy the public DNS shown when checking the services and paste it in a browser to check you have access to nginx.

![Screenshot of pipeline.](/images/nginx.jpg)